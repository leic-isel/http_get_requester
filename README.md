# rcp/HTTP_GET_requester



## Getting Started

Download links:

SSH clone URL: git clone git@bitbucket.org:leic-isel/http_get_requester.git

HTTPS clone URL: git clone https://oicnanev@bitbucket.org/leic-isel/http_get_requester.git



These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

What things you need to install the software and how to install them.

```
Install git for windows https://gitforwindows.org/ or git for Mac https://git-scm.com/download/mac

Then in git bash (windows) or terminal (Mac) type: git clone https://git.jetbrains.space/vencode/rcp/HTTP_GET_requester.git

Download python from https://www.python.org
When installing in windows, choose the option Add Python 3.10 to PATH
```

## Run App

To run the app (cmd in windows, terminal in mac) type:
python3 http_GET_request.py

## Resources

Python socket: https://docs.python.org/3/library/socket.html

HTTP Status Messages: https://www.w3schools.com/tags/ref_httpmessages.asp

List of Top Level Domains: https://data.iana.org/TLD/tlds-alpha-by-domain.txt

ASCII ART for banner: http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20
