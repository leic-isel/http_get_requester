""" ###########################################################################
 ISEL - Instituto Superior de Engenharia de Lisboa.

 LEIC - Licenciatura em Engenharia Informatica e de Computadores.

 RCp  - Redes de Computador

 Trabalho Prático - Fase 1
 45837 Miguel Queluz
 46372 Hugo Araújo
 45824 Nuno Venâncio

 http-GET-requester.py

 WEBOGRAPHY:
 Python socket: https://docs.python.org/3/library/socket.html
 HTTP Status Messages: https://www.w3schools.com/tags/ref_httpmessages.asp
 List of Top Level Domains: https://data.iana.org/TLD/tlds-alpha-by-domain.txt
 ASCII ART for banner: http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20
########################################################################### """

import socket
import os

from top_level_domains import TLD
from html_status_codes import STATCODE


class Controller(object):
	def __init__(self):
		self.view = View()
		self.model = Model()
		self.hostname = ''
		self.want_full_reply = False
		self.full_reply = ''

	def run(self):
		# show banner
		self.view.show_banner()

		# start infinite loop
		while True:
			# create socket
			self.model.set_socket()

			# get hostname
			while '.' not in self.hostname or self.hostname[self.hostname.rfind('.'):] not in TLD:
				self.view.set_hostname()
				self.hostname = self.view.get_hostname()

				# Out of infinite condition and exit application
				if self.hostname == 'q':  # inner while loop
					break
			if self.hostname == 'q':  # outer while loop
				break

			# Connect our socket to the hostname remote socket on port 80
			self.model.connect(self.hostname)

			# Get remote host IP
			host_IPv4 = self.model.get_host_IPv4(self.hostname)

			# Send GET request to remote socket
			self.model.send_GET(self.hostname)

			# Get HTTP status code
			stat_code = self.model.get_status_code()

			# Show user the result
			self.view.show_result(stat_code, host_IPv4)
			self.hostname = ''  # empty hostname for next round

			# Check if user wants see full http reply
			if self.view.see_full_reply() == 'y':
				self.want_full_reply = True
				self.full_reply = self.model.get_reply()

			# Get full http reply
			if self.want_full_reply:
				self.view.show_full_reply(self.full_reply)
			self.full_reply = ''  # clear full reply


class Model(object):	
	def __init__(self):
		self.socket = None
		self.host_IPv4 = ''
		self.reply = ''

	def set_socket(self):
		family = socket.AF_INET # socket address family (INET IPv4)
		soc_type = socket.SOCK_STREAM # socket type (Stream)
		self.socket = socket.socket(family, soc_type)

	def connect(self, hostname):
		self.socket.connect((hostname, 80))

	def get_host_IPv4(self, hostname):
		self.host_IPv4 = socket.gethostbyname(hostname)

		return self.host_IPv4

	def send_GET(self, hostname):
		get_msg = f"GET / HTTP/1.1\r\nHost: {hostname}\r\n\r\n"
		
		# the GET request must be sent in binary (encoded)
		self.socket.sendall(get_msg.encode())
		self.set_reply()

	def set_reply(self):
		bufsize = 4096  # socket buffer size to receive data
		self.reply = self.socket.recv(bufsize)

	def get_status_code(self):
		# Get the integer of the status code. 
		# Need to substring the response from position 9 to 11, decode because response is in bytes 
		# and then, cast it to int
		stat_code = int(self.reply[9:12].decode())

		return stat_code

	def get_reply(self):
		return self.reply.decode()


class View(object):
	def __init__(self):
		self.hostname = ''

	def set_hostname(self):
		self.hostname = input("\nWhat's the hostname you want to reach?\n")

	def get_hostname(self):
		return self.hostname

	def show_banner(self):
		# absolute directory path where this script is executed
		path_dir = os.path.dirname(os.path.abspath(__file__))

		# open and print the file banner.txt (must be in the same directory
		# of this script) use the keyword 'with' so the stream is automatically
		# closed after use
		with open(f'{path_dir}{os.sep}banner.txt', 'r', encoding='utf8') as file:
			line = file.readline()
			while line:
				print(line, end='')
				line = file.readline()

	def show_result(self, status_code, host_IPv4):
		print()
		print(f"The server {self.hostname} with IP: {host_IPv4} has responded with:")
		print(STATCODE[status_code])
		print()

	def see_full_reply(self):
		while True:
			full = input("Want to see the full request reply? (y or n)\n")
			if full == 'y' or full == 'n':
				break

		return full

	def show_full_reply(self, full_reply):
		print()
		print(full_reply)
		print()


# Instantiate app and run it --------------------------------------------------------
if __name__ == '__main__':
	app = Controller()
	app.run()
