# Dictionary (similar to Java's Hash map) of HTTP status messages
STATCODE = {
    # 1xx: INFORMATION ###########################
    100: "100 Continue\nThe server has received the request headers, and the client should proceed to the request body",
    101: "101 Switching Protocols\nThe requester has asked the server to switch protocols",
    103: "103 Checkpoint\nUsed in the resumable request proposal to resume aborted PUT or POST requests",

    # 2xx: SUCCESSFUL ############################
    200: "200 OK\nThe request is OK (standard response for successfull HTTP requests)",
    201: "201 Created\nThe request has been fulfilled, and a new resource is created",
    202: "202 Accepted\nThe request has been accepted for processing, but the processing has not been completed",
    203: "203 Non-Authoritative Information\nThe request has been successfully processed, but is returning information that may be from\nanother source",
    204: "204 No Content\nThe request has been successfully processed, but is not returning any content",
    205: "Reset Content\nThe request has been successfully processed, but is not returning any content, and requires\nthat the requester reset the document view",
    206: "206 Partial Content\nThe server is delivering only part of the resource due to a range header sent by the client",

    # 3xx: REDIRECTION ############################
    300: "300 Multiple Choices\nA link list. The user can select a link and go to that location. Maximum five addresses",
    301: "301 Moved Permanently \n	The requested page has moved to a new URL",
    302: "302 Found\nThe requested page has moved temporarily to a new URL",
    303: "303 See Other\nThe requested page can be found under a different URL",
    304: "304 Not Modified \nIndicates the requested page has not been modified since last requested",
    307: "307 Temporary Redirect\nThe requested page has moved temporarily to a new URL",
    308: "308 Resume Incomplete\nUsed in the resumable requests proposal to resume aborted PUT or POST requests",

    # 4xx: CLIENT ERROR ###########################
    400: "400 Bad Request\nThe request cannot be fulfilled due to bad syntax",
    401: "401 Unauthorized\nThe request was a legal request, but the server is refusing to respond to it. For use when \nauthentication is possible but has failed or not yet been provided",
    402: "402 Payment Required\nReserved for future use",
    403: "403 Forbidden\nThe request was a legal request, but the server is refusing to respond to it",
    404: "404 Not Found\nThe requested page could not be found but may be available again in the future",
    405: "405 Method Not Allowed\n	A request was made of a page using a request method not supported by that page",
    406: "406 Not Acceptable\nThe server can only generate a response that is not accepted by the client",
    407: "407 Proxy Authentication Required\nThe client must first authenticate itself with the proxy",
    408: "408 Request Timeout\nThe server timed out waiting for the request",
    409: "409 Conflict\nThe request could not be completed because of a conflict in the request",
    410: "410 Gone\nThe requested page is no longer available",
    411: '411 Length Required\nThe "Content-Length" is not defined. The server will not accept the request without it',
    412: "412 Precondition Failed\nThe precondition given in the request evaluated to false by the server",
    413: "413 Request Entity Too Large\nThe server will not accept the request, because the request entity is too large",
    414: "414 Request-URI Too Long\nThe server will not accept the request, because the URL is too long. Occurs when you convert\na POST request to a GET request with a long query information",
    415: "Unsupported Media Type\nThe server will not accept the request, because the media type is not supported",
    416: "416 Requested Range Not Satisfiable\nThe client has asked for a portion of the file, but the server cannot supply that portion",
    417: "417 Expectation Failed\nThe server cannot meet the requirements of the Expect request-header field",

    # 5xx: SERVER ERROR ###########################
    500: "500 Internal Server Error\nA generic error message, given when no more specific message is suitable",
    501: "501 Not Implemented\nThe server either does not recognize the request method, or it lacks the ability to fulfill \nthe request",
    502: "502 Bad Gateway\nThe server was acting as a gateway or proxy and received an invalid response from the \nupstream server",
    503: "503 Service Unavailable\nThe server is currently unavailable (overloaded or down)",
    504: "504 Gateway Timeout\nThe server was acting as a gateway or proxy and did not receive a timely response from \nthe upstream server",
    505: "505 HTTP Version Not Supported\nThe server does not support the HTTP protocol version used in the request",
    511: "511 Network Authentication Required\nThe client needs to authenticate to gain network access"
}